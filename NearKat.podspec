#
# Be sure to run `pod lib lint NearKat.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "NearKat"
  s.version          = "0.1.1"
  s.summary          = "NearKat provides a way for your application to know more about your users and what they need."
  s.description      = <<-DESC
		       	NearKat provides a way for your application to know more about your users and what they need.
                       
			* Mobile-to-Store Attribution
                       	* Audience Understanding
                       DESC
  s.homepage         = "http://neark.at"
  s.license          = 'MIT'
  s.author           = { "Jared Dean" => "jdean@stepleaderdigital.com", "Sean Doherty" => "sean.doherty@crosscomm.net" }
  s.source           = { :git => "https://bitbucket.org/nearkat_sdk/nearkat-ios.git", :tag => s.version.to_s }
  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'CoreBluetooth', 'CoreLocation'
  s.dependency 'iHasApp', '~> 2.2'
end
