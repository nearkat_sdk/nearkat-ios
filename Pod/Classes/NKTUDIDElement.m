//
//  UDIDElement.m
//
//  Created by David Oldis on 3/26/12.
//  Copyright 2012 StepLeader Inc. All rights reserved.
//

#import "NKTUDIDElement.h"
#import "NSString+trim.h"

@interface NKTUDIDElement (private)

@end

@implementation NKTUDIDElement

+ (NSString *) getUDID {
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

@end
