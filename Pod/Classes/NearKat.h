//
//  NearKatSDK.h
//  NearKatSDK
//
//  Created by Sean Doherty on 1/8/2015.
//  Copyright (c) 2015 StepLeader Digtial. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, NKServiceType) {
    NKServiceTypeSandbox,
    NKServiceTypeProduction
};

@interface NearKat : NSObject

// SDK singleton.  All SDK access should occur through this object.
+ (NearKat*) sharedInstance;

-(id) setupWithAPIKey:(NSString*) key;
-(id) setupWithAPIKey:(NSString*) key andServiceType:(NKServiceType) serviceType;

@property (assign,nonatomic) NKServiceType serviceType;

// Array of strings, each containing a UUID.  These UUIDs
// will override the list retrieved from the NearKat
// server.  This is useful to debug/verify that the SDK
// can detect a known iBeacon when testing.  This is a
// development feature and should not be used in production.
// In order to override the UUIDs from the server, this
// property should be set before starting the service.
@property (nonatomic, strong) NSArray *debugUUIDs;

// Debug flag for the SDK.  If this value is YES, the SDK
// will log debugging information to the console.
// Default value is NO.
// This can be toggled during the lifetime of the SDK usage.
@property (nonatomic, assign) BOOL debug;

// Accessor properties for the SDK.
// At any time, the client can access the list of errors
// and the list of personas.  Both are arrays of NSStrings.
// Values may be nil.
@property (nonatomic, strong) NSArray *personas;
@property (nonatomic, strong) NSArray *errors;

// Start the SDK service.  The SDK will contact the API and retrieve
// further configuration info.  Background beacon scanning will begin
// and beacons will be logged via the API.
- (void) start;

@end
