//
//  NKTBlueToothManager.h
//  NearKat
//
//  Created by Jay Lyerly on 5/19/14.
//  Copyright (c) 2014 StepLeader Digital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^NKTBlueToothStatusBlock)(CBCentralManagerState state);
typedef void (^NKTBlueToothBeaconBlock)(NSArray *beacons);
typedef void (^NKTBlueToothRegionBlock)(CLRegion *region, BOOL entered);

@interface NKTBlueToothManager : NSObject
@property (nonatomic, copy, readonly)   NSString                *status;
@property (nonatomic, readonly)         BOOL                    hasBluetooth;

@property (nonatomic, assign, readonly) CLLocationCoordinate2D  userCoordinate;
@property (nonatomic, strong, readonly) CLLocation              *userLocation;
@property (nonatomic, strong, readonly) CLPlacemark             *userPlacemark;

+ (NKTBlueToothManager *) sharedMgr;
- (void) addBeacon:(NSString *)beaconID;
- (void) addStatusBlock:(NKTBlueToothStatusBlock)block;
- (void) addBeaconBlock:(NKTBlueToothBeaconBlock)block;
- (void) addRegionBlock:(NKTBlueToothRegionBlock)block;
- (void) shutdownMonitor;

@end
