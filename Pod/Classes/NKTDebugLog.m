//
//  NKTDebugLog.m
//  NearKatSDK
//
//  Created by Jay Lyerly on 10/20/14.
//  Copyright (c) 2014 StepLeader Digtial. All rights reserved.
//

#import "NKTDebugLog.h"
#import "NearKat.h"

void NKTLog(NSString *format, ...) {
    va_list arguments;
    va_start(arguments, format);
    
    NSString *formattedString = [[NSString alloc] initWithFormat:format arguments:arguments];
    [[NKTDebugLog sharedNearKatDebugLog] logString:formattedString];
    
    va_end(arguments);
}

@interface NKTDebugLog ()
@property (nonatomic, readonly)     BOOL    enabled;
@end

@implementation NKTDebugLog

+ (NKTDebugLog *) sharedNearKatDebugLog {
    static NKTDebugLog *_mgr;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _mgr = [[NKTDebugLog alloc] init];
    });
    
    return _mgr;
}

- (void) logString:(NSString *)aString {
    if (self.enabled){
        NSLog(@"NEARKAT: %@", aString);
    }
}

- (BOOL) enabled{
    return [NearKat sharedInstance].debug;
}

@end
