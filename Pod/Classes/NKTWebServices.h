//
//  NKTWebServices.h
//  NearKatSDK
//
//  Created by Sean Doherty on 1/7/15.
//  Copyright (c) 2015 StepLeader Digtial. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NKTBeacon.h"

@interface NKTWebServices : NSObject
+(void) setApiKey:(NSString *)apiKey;
+(void) setApiUrl:(NSString *)apiUrl;
+(void) registerDeviceWithResult:(void (^)(BOOL success, NSDictionary* result, NSError* error))result;
+(void) sendNotificationOfBeacon:(NKTBeacon*) beacon
        result:(void (^)(BOOL success, NSDictionary* result, NSError* error))result;
+(void) sendInstalledAppIds:(NSArray*) appIds
        result:(void (^)(BOOL success, NSDictionary* result, NSError* error))result;
@end

