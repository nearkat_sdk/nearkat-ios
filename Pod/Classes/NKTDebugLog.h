//
//  NKTDebugLog.h
//  NearKatSDK
//
//  Created by Jay Lyerly on 10/20/14.
//  Copyright (c) 2014 StepLeader Digtial. All rights reserved.
//

#import <Foundation/Foundation.h>

void NKTLog(NSString *format, ...) NS_FORMAT_FUNCTION(1,2);

@interface NKTDebugLog : NSObject

+ (instancetype)sharedNearKatDebugLog;

- (void)logString:(NSString *)aString;

@end
