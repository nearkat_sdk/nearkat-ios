//
//  NKTBeaconLogger.h
//  IBeaconDemo
//
//  Created by Jay Lyerly on 5/20/14.
//  Copyright (c) 2014 StepLeader Digital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class NKTBeacon;
@class NKTBeaconLogger;

@protocol NKTBeaconLoggerWatcher <NSObject>
@optional
- (void) beaconLogger:(NKTBeaconLogger *)logger didLogMessage:(NSString *)msg date:(NSDate *)date;
- (void) beaconLogger:(NKTBeaconLogger *)logger didLogBeacon:(NKTBeacon *)beacon date:(NSDate *)date;
@end

@interface NKTBeaconLogger : NSObject
+ (NKTBeaconLogger *) sharedMgr;

- (void)logBeacon:(CLBeacon *)beacon;
- (void)logBeaconRegion:(CLBeaconRegion *)beaconRegion;
- (void)addWatcher:(id<NKTBeaconLoggerWatcher>)watcher;

@end
