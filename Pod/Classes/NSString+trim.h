//
//  NSString+trim.h
//
//  Copyright 2012 StepLeader Inc. All rights reserved.
//


@interface NSString (trim)

+ (NSString *)nkt_trim:(NSString *)original;
- (NSString *)nkt_trimString;
- (BOOL) nkt_isNotEmpty;
- (BOOL) nkt_isEmpty;
- (NSString *) nkt_md5Hash32;
- (NSString *) nkt_md5HashWithSalt32: (NSString*) salt;
- (NSString *) nkt_md5Hash64;
- (NSString *) nkt_md5HashWithSalt64: (NSString*) salt;

@end
