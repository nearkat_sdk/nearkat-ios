//
//  NKTBeaconLogger.m
//  IBeaconDemo
//
//  Created by Jay Lyerly on 5/20/14.
//  Copyright (c) 2014 StepLeader Digital. All rights reserved.
//

#import "NKTBeaconLogger.h"
#import "NKTBeacon.h"
#import "CompilerMacros.h"
#import "NKTDebugLog.h"

@interface NKTBeaconLogger ()
@property (nonatomic, strong) NSMutableArray *watchers;
@end

@implementation NKTBeaconLogger

+ (NKTBeaconLogger *) sharedMgr {
    static NKTBeaconLogger *_mgr;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _mgr = [[NKTBeaconLogger alloc] init];
    });
    
    return _mgr;
}

- (instancetype) init {
    self = [super init];
    if (self){
        _watchers = [ @[] mutableCopy ];
    }
    return self;
}

- (void)logBeaconRegion:(CLBeaconRegion *)beaconRegion {
    [self logNktBeacon:[[NKTBeacon alloc] initWithBeaconRegion:beaconRegion]];
}


- (void)logBeacon:(CLBeacon *)beacon {
    [self logNktBeacon:[[NKTBeacon alloc] initWithBeacon:beacon]];
}

- (void)logNktBeacon:(NKTBeacon *)beacon {
    NSMutableString *logString = [@"" mutableCopy];
    [logString appendFormat:@"Saw Beacon: %@:%@ %@", beacon.major, beacon.minor, [beacon.proximityUUID UUIDString]];
    NKTLog(@"%@", logString);
    
    for (id watcher in self.watchers){
        if ([watcher respondsToSelector:@selector(beaconLogger:didLogMessage:date:)]){
            [watcher beaconLogger:self didLogMessage:logString date:[NSDate date]];
        }
        if ([watcher respondsToSelector:@selector(beaconLogger:didLogBeacon:date:)]){
            [watcher beaconLogger:self didLogBeacon:beacon date:[NSDate date]];
        }
    }
}

- (void)addWatcher:(id<NKTBeaconLoggerWatcher>)watcher {
    [self.watchers addObject:watcher];
}

@end
