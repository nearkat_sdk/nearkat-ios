//
//  NearKatSDK.m
//  NearKatSDK
//
//  Created by Sean Doherty on 1/8/2015.
//  Copyright (c) 2015 StepLeader Digtial. All rights reserved.
//

#import "NearKat.h"
#import <UIKit/UIKit.h>
#import "NKTBlueToothManager.h"
#import "NKTWebServices.h"
#import "CompilerMacros.h"
#import "iHasApp.h"
#import "NKTDebugLog.h"



@interface NearKat (PrivateMethods)
@end

@implementation NearKat
NSString * const kNearKatBaseURLSandbox = @"http://sandboxsdk.neark.at/";
NSString * const kNearKatBaseURLProduction = @"https://sdk.neark.at/";
static NearKat *_sharedInstance;

- (instancetype) init {
    self = [super init];
    if (self){
        
    }
    return self;
}

- (void) dealloc {
    
}

+(NearKat*) sharedInstance {
    // refuse to initialize unless we're at iOS 7 or later.
    if ([[[UIDevice currentDevice] systemVersion] integerValue] < 7){
        return nil;
    }
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[NearKat alloc] init];
    });
    
    return  _sharedInstance;
}

-(id) setupWithAPIKey:(NSString*) key {
    return [self setupWithAPIKey:key andServiceType:NKServiceTypeProduction];
}

-(id) setupWithAPIKey:(NSString*) key andServiceType:(NKServiceType) serviceType {
    [NKTWebServices setApiKey:key];
    //Set service type value and update webservices with appropriate base url
    self.serviceType = serviceType;
    if (serviceType == NKServiceTypeSandbox){
        [NKTWebServices setApiUrl:kNearKatBaseURLSandbox];

    } else {
        [NKTWebServices setApiUrl:kNearKatBaseURLProduction];
    }
    
    return [NearKat sharedInstance];
}

- (void) registerDevice {
    NKTLog(@"Registering device with Server");
    [NKTWebServices registerDeviceWithResult:^(BOOL success, NSDictionary *result, NSError *error) {
        if (success){
            NKTLog(@"Device registered successfully");
            self.personas = objc_dynamic_cast(NSArray, [result objectForKey:@"personas"]);
            
            //Only start scanning if server returns discovery_enabled = true
            if ([objc_dynamic_cast(NSNumber, result[@"discovery_enabled"]) boolValue]){
                //If we have debug UUID's set, ignore list from server
                if (self.debugUUIDs) {
                    [self startBeaconScanningForBeacons:self.debugUUIDs];
                } else {
                    NSArray *beaconsToScan = objc_dynamic_cast(NSArray, result[@"beacons"]);
                    [self startBeaconScanningForBeacons:beaconsToScan];
                }
            
                //Start scanning for installed apps
                [self findInstalledApps];
            }

        } else {
            NKTLog(@"Device registration failed");
            self.errors = objc_dynamic_cast(NSArray, [result objectForKey:@"errors"]);
        }
    }];
}

- (void) findInstalledApps {
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NKTLog(@"Scanning device for applications");
        iHasApp *detectionObject = [[iHasApp alloc] init];
        [detectionObject detectAppIdsWithIncremental:nil withSuccess:^(NSArray *appIds) {
            NKTLog(@"Sending scanned applications to Server");
            [NKTWebServices sendInstalledAppIds:appIds result:^(BOOL success, NSDictionary *result, NSError *error) {
                if (success) {
                    NKTLog(@"Scan results sent successfully");
                    self.personas = objc_dynamic_cast(NSArray, [result objectForKey:@"personas"]);
                } else {
                    NKTLog(@"Scan results sending failed");
                    self.errors = objc_dynamic_cast(NSArray, [result objectForKey:@"errors"]);
                }
            }];
        } withFailure:^(NSError *error) {
            NKTLog(@"Failure with beacon scanning: %@", error.localizedDescription);
        }];
    });
}

-(void) startBeaconScanningForBeacons: (NSArray*) beacons {
    NKTLog(@"Starting beacon scanning");
    for (NSString *uuid in beacons){
        NKTLog(@"Scanning for beacons with UUID: %@", uuid);
        [[NKTBlueToothManager sharedMgr] addBeacon:uuid];
    }
    
}

- (void)start {
    if ([CBCentralManager instancesRespondToSelector:@selector(initWithDelegate:queue:options:)]) {
        // enable beacon scanning if this device supports Bluetooth LE
        [[NKTBlueToothManager sharedMgr] addStatusBlock:^(CBCentralManagerState state){
            // don't connect to the endpoint until the bluetooth status is ready
            static BOOL firstTime = YES;
            
            if (firstTime){
                [self registerDevice];
                firstTime = NO;
            };
        }];
    } else {
        [self registerDevice];
    }
}

@end
