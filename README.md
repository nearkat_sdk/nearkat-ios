# NearKat

[![CI Status](http://img.shields.io/travis/Sean Doherty/NearKat.svg?style=flat)](https://travis-ci.org/Sean Doherty/NearKat)
[![Version](https://img.shields.io/cocoapods/v/NearKat.svg?style=flat)](http://cocoadocs.org/docsets/NearKat)
[![License](https://img.shields.io/cocoapods/l/NearKat.svg?style=flat)](http://cocoadocs.org/docsets/NearKat)
[![Platform](https://img.shields.io/cocoapods/p/NearKat.svg?style=flat)](http://cocoadocs.org/docsets/NearKat)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NearKat is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "NearKat"

## Author

Sean Doherty, sean.doherty@crosscomm.net

## License

NearKat is available under the MIT license. See the LICENSE file for more info.

