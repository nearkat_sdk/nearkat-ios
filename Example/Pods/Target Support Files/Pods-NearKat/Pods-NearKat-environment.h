
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// NearKat
#define COCOAPODS_POD_AVAILABLE_NearKat
#define COCOAPODS_VERSION_MAJOR_NearKat 0
#define COCOAPODS_VERSION_MINOR_NearKat 1
#define COCOAPODS_VERSION_PATCH_NearKat 0

// iHasApp
#define COCOAPODS_POD_AVAILABLE_iHasApp
#define COCOAPODS_VERSION_MAJOR_iHasApp 2
#define COCOAPODS_VERSION_MINOR_iHasApp 2
#define COCOAPODS_VERSION_PATCH_iHasApp 0

