//
//  main.m
//  NearKat
//
//  Created by Sean Doherty on 01/09/2015.
//  Copyright (c) 2014 Sean Doherty. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NKTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NKTAppDelegate class]));
    }
}
