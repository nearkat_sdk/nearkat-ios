//
//  NKTViewController.m
//  NearKat
//
//  Created by Sean Doherty on 01/09/2015.
//  Copyright (c) 2014 Sean Doherty. All rights reserved.
//

#import "NKTViewController.h"
#import <NearKat.h>

@interface NKTViewController ()

@end

@implementation NKTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // API configuration values
    NSString *apiKey = @"53beb58e5e0a864201e61c56";
    
    // Get a reference to the SDK object
    NearKat *theSDK = [[NearKat sharedInstance] setupWithAPIKey:apiKey andServiceType:NKServiceTypeProduction];
    
    // Turn on debug logging, not for production
    theSDK.debug = YES;
    
    // Override the beacon list for testing with known local beacons.
    theSDK.debugUUIDs = @[ @"B9407F30-F5F8-466E-AFF9-25556B57FE6D"];
    
    // Once the config values are set, start the SDK.
    // The SDK will contact the server for further config info
    // and start monitoring for beacons.
    [theSDK start];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
