//
//  NKTAppDelegate.h
//  NearKat
//
//  Created by CocoaPods on 01/09/2015.
//  Copyright (c) 2014 Sean Doherty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NKTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
